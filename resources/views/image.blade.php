@extends('layouts.app')
@section('title', '图片处理')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12" style="margin-top: 40px">
      @include('common.message')
      <h1 class="text-center" style="margin-bottom: 40px">阿里云-OSS-图片处理</h1>
      <form method="post" enctype="multipart/form-data" action="{{ route('image.upload') }}">
      	{{ csrf_field() }}
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">待打水印目标图片</label>
          <div class="col-sm-10">
            <input type="file" class="form-control" name="original_image">
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">文字水印</label>
          <div class="col-sm-10">
            <label class="custom-control custom-radio">
              <input value="1" name="text_mask" type="radio" class="custom-control-input" {{ old("text_mask", 1) == 1 ? "checked" : "" }}>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">使用</span>
            </label>
            <label class="custom-control custom-radio">
              <input value="0" name="text_mask" type="radio" class="custom-control-input" {{ old("text_mask", 1) == 0 ? "checked" : "" }}>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">不使用</span>
            </label>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">水印文字</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="text" placeholder="水印的文字"j value="{{ old('text') }}">
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">位置</label>
          <div class="col-sm-10">
            <select class="custom-select" name="text_position">
              @foreach($positions as $k => $v)
              <option value="{{ $k }}" {{ old("text_position", "fill") == $k ? "selected" : ""}}>{{ $v }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">文字旋转角度</label>
          <div class="col-sm-10">
            <input type="number" min="0" max="360" value="{{ old('text_rotate', 30) }}" class="form-control" name="text_rotate" placeholder="顺时针，0~360度">
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">图片水印</label>
          <div class="col-sm-10">
            <label class="custom-control custom-radio">
              <input value="1" name="image_mask" type="radio" class="custom-control-input" {{ old("image_mask", 1) == 1 ? "checked" : "" }}>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">使用</span>
            </label>
            <label class="custom-control custom-radio">
              <input value="0" name="image_mask" type="radio" class="custom-control-input" {{ old("image_mask", 1) == 0 ? "checked" : "" }}>
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">不使用</span>
            </label>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">水印图片</label>
          <div class="col-sm-10">
            <input type="file" class="form-control" name="image" placeholder="水印的文字">
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label">位置</label>
          <div class="col-sm-10">
            <select class="custom-select" name="image_position">
              {{ array_pop($positions) }}
              @foreach($positions as $k => $v)
              <option value="{{ $k }}" {{ old("image_position", "se") == $k ? "selected" : ""}}>{{ $v }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-2 form-control-label"></label>
          <div class="col-sm-10">
            <input type="submit" class="btn btn-primary" value="提交" style="cursor: pointer">
          </div>
        </div>
      </form>
    </div>
  </div>
  @if(session()->has("mask_image"))
  <div class="row">
  	<div class="col-md-6">
      	<div class="card">
          <img class="card-img-top" src="{{ session()->get('original_image') }}" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">原图URL: <a href="{{ session()->get('original_image') }}" target="_blank">{{ session()->get('original_image') }}</a></p>
          </div>
        </div>
    </div>
    <div class="col-md-6">
      	<div class="card">
          <img class="card-img-top" src="{{ session()->get('mask_image')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">水印图URL: <a href="{{ session()->get('mask_image') }}" target="_blank">{{ session()->get('mask_image') }}</a></p>
          </div>
        </div>
    </div>
  </div>
  @endif
</div>
@stop