<?php

return [
    'access_key_id' => env('OSS_ACCESS_KEY_ID'),
    
    'access_key_secret' => env('OSS_ACCESS_KEY_SECRET'),
    
    'endpoint' => env('OSS_ENDPOINT'),
];